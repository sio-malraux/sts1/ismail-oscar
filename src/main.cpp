#include <libpq-fe.h>
#include <iostream>
#include <string>

int main()
{
  int code_retour;
  code_retour = 0;
  PGPing code_retour_ping;
  char coninfo[] = "host=postgresql.bts-malraux72.net port=5432 user=i.uysak password=P@ssword";
  code_retour_ping = PQping(coninfo);
  PGconn *connexion ;

  if(code_retour_ping == PQPING_OK)
  {
    std::cout << "Le serveur est en cours d'exécution et semble accepter les connexions" << std::endl;
    connexion = PQconnectdb(coninfo);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      std::cout << "La connexion au serveur de base de donnée "
                << PQhost(connexion)
                << " a été établie avec les paramètres suivants : "
                << std::endl;
      std::cout << "* utilisateur :"
                << PQuser(connexion)
                << std::endl;
      std::cout << "* mot de passe : " ;
                
      
      int strlenght = sizeof PQpass(connexion); 

      for (int i = 0; i < strlenght ; i++)

      {
        std::cout << "*";
      }

      std::cout << std::endl;
      
      std::cout << "* base de données : "
                << PQdb(connexion)
                << std ::endl;
      std::cout << "* port :"
                << PQport(connexion)
                << std::endl;
      std::cout << "* chiffrement SSL : ";

      int PQssl = PQsslInUse(connexion);
      if (PQssl ==1)
      {
        std::cout << " true ";
      }
      else
      {
        std::cout << " false ";
      }
      
      std::cout << std::endl;
      
      std::cout << "* encodage : "
                << PQparameterStatus(connexion, "server_encoding")        
                << std::endl;
      std::cout << "* version du protocole : "
                << PQprotocolVersion(connexion)
                << std::endl;
      std::cout << "* version du serveur : "
                << PQserverVersion(connexion)
                << std::endl;
      std::cout << "* version de la bibliothèque 'libpq' du client : "
                << PQlibVersion()
                << std::endl;
    }
    else if(PQstatus(connexion) == CONNECTION_BAD)
    {
      std::cerr << "La tentative de connexion à échouée" << std::endl; 
    }
  }
  else
  {
    std::cerr << "Le serveur ne semble pas être accessible. Veuillez contacter votre administrateur" << std::endl;
    code_retour = 1;
  }

  //Récupération de données
  //Commande récupérée sur Monsieur G.DAVID car PgAdmin ne fonctionne pas
 const char *requete_sql = "SELECT * FROM si6.\"Animal\" JOIN si6.\"Race\" on si6.\"Animal\".race_id = si6.\"Race\".id WHERE si6.\"Animal\".sexe = 'Femelle' AND si6.\"Race\".nom = 'Singapura';";
  PGresult *resultats = PQexec(connexion, requete_sql);


  ExecStatusType etat=PQresultStatus(resultats);
    if (etat == PGRES_EMPTY_QUERY)
    {
      std::cout << "La chaîne envoyé au serveur était vide"
                << std::endl;
    }
    else if (etat == PGRES_COMMAND_OK)
      {
        std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée"
                  << std::endl;
      }
    else if (etat == PGRES_TUPLES_OK)
    {
      std::cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)"
                << std::endl;
        }    
    else if (etat == PGRES_COPY_OUT)
    {
      std::cout << " Début de l'envoi (à partir du serveur) d'un flux de données" << std::endl;
    }
    else if (etat == PGRES_COPY_IN)
    {
      std::cout << " Début de la réception (sur le serveur) d'un flux de données "
                << std::endl;
    }
    else if (etat == PGRES_BAD_RESPONSE)
    {
      std::cout << " La réponse du serveur n'a pas été comprise "
                << std::endl;
    }
    else if (etat == PGRES_NONFATAL_ERROR)
    {
      std::cout << "Une erreur non fatale (une note ou un avertissement) est survenue"
                <<std::endl;
    }
    else if (etat == PGRES_FATAL_ERROR)
    {
      std::cout << "Une erreur fatale est survenue "
                <<std::endl;
    }
    else if (etat == PGRES_COPY_BOTH)
    {
      std::cout << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, so this status should not occur in ordinary applications. "
                << std::endl;
    }
      
    

  return code_retour;
}
